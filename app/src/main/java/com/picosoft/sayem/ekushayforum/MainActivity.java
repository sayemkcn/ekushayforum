package com.picosoft.sayem.ekushayforum;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.CookieManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;


public class MainActivity extends ActionBarActivity {
    WebView myWebView;
    ProgressDialog openingProgressDialog;
    final Context context = MainActivity.this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        CookieManager.getInstance().setAcceptCookie(true);
        myWebView = (WebView) this.findViewById(R.id.webView);
        // Load Ads
        AdView mAdView = (AdView) this.findViewById(R.id.bannerAdView);
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice("7F04F2ED5A2045BEF2F88A5D88F2EDD1")
                .build();
        mAdView.loadAd(adRequest);
        // End Ad Loading
        // ProgressDialog when opening app.
        openingProgressDialog = new ProgressDialog(context);
        openingProgressDialog.setMessage("Loading..");
        openingProgressDialog.show();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                try {
                    openingProgressDialog.dismiss();
                } catch (Exception e) {
                    openingProgressDialog.dismiss();
                }
            }
        }, 5000);

        //enable Javascript
        myWebView.getSettings().setJavaScriptEnabled(true);

        //loads the WebView completely zoomed out
        myWebView.getSettings().setLoadWithOverviewMode(true);

        //true makes the Webview have a normal viewport such as a normal desktop browser
        //when false the webview will have a viewport constrained to it's own dimensions
        myWebView.getSettings().setUseWideViewPort(true);

        //override the web client to open all links in the same webview
        myWebView.setWebViewClient(new WebViewClient() {
            ProgressDialog progressDialog;

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                progressDialog = new ProgressDialog(context);
                progressDialog.setMessage("Loading..");

                progressDialog.show();
                view.loadUrl(url);
                return false;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        try {
                            progressDialog.dismiss();
                        } catch (Exception e) {

                        }
                    }
                }, 3000);
            }
        });


        //Injects the supplied Java object into this WebView. The object is injected into the
        //JavaScript context of the main frame, using the supplied name. This allows the
        //Java object's public methods to be accessed from JavaScript.
//        myWebView.addJavascriptInterface(new JavaScriptInterface(this), "Android");

        //load the home page URL
        myWebView.loadUrl("http://www.ekushay.com");


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK:
                    if (myWebView.canGoBack()) {
                        myWebView.goBack();
                    } else {
                        finish();
                    }
                    return true;
            }

        }
        return super.onKeyDown(keyCode, event);
    }
}
